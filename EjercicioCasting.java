package com.ejercicios.casting;

import java.util.ArrayList;
import java.util.stream.Stream;

public class EjercicioCasting {

	public static void main(String[] args) {
		
		//convertir de String a int
		String a= "123";
		int b = Integer.parseInt(a);
		System.out.println(b);
		//convertir de int a double
		int c = 1;
		double d = (int)c;
		System.out.println(d);
		//convertir de double a String
		Double e = new Double(3.6873);
		String f = String.valueOf(e);
		System.out.println(f);
		//convertir de String a short
		String g = "15";
		short h = Short.parseShort(g);
		System.out.println(h);
		//convertir de int a integer
		int i = 2;
		Integer j = new Integer(i);
		System.out.println(j);
		//convertir de Double a double
		Double k = new Double(1.22531);
		double m = k.doubleValue();
		System.out.println(m);
		
		
		
	}

}
